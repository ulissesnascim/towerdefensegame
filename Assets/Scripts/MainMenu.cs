﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string levelToLoad = "MainLevel";

    public SceneFader sceneFader;

    private void Start()
    {
        //Use this to reset all levels to locked
        //PlayerPrefs.SetInt("levelReached", 1);

    }

    public void Play()
    {
        sceneFader.FadeTo(levelToLoad);

    }

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();

    }


}
