﻿using UnityEngine;
using UnityEngine.UI;


public class Shop : MonoBehaviour {

    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserBeamer;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
        GameObject standardTurretItem = gameObject.transform.Find("StandardTurretItem").gameObject;
        GameObject missileLauncherItem = gameObject.transform.Find("MissileLauncherItem").gameObject;
        GameObject laserBeamItem = gameObject.transform.Find("LaserBeamerItem").gameObject;

        standardTurretItem.SetActive(standardTurret.enabledInScene);
        missileLauncherItem.SetActive(missileLauncher.enabledInScene);
        laserBeamItem.SetActive(laserBeamer.enabledInScene);


    }

    public void SelectStandardTurret()
    {
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectMissileLauncher()
    {
        buildManager.SelectTurretToBuild(missileLauncher);
    }

    public void SelectLaserBeamer()
    {
        buildManager.SelectTurretToBuild(laserBeamer);
    }

    }
