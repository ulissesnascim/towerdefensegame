﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WavesSurvived : MonoBehaviour {

    public Text numberText;

    void OnEnable()
    {
        numberText.text = PlayerStats.EnemiesKilled.ToString();
        StartCoroutine(AnimateText());
    }

    IEnumerator AnimateText()
    {
        numberText.text = "0";
        int number = 0;

        yield return new WaitForSeconds(.7f);

        while (number < PlayerStats.EnemiesKilled)
        {

            number++;
            numberText.text = number.ToString();
            yield return new WaitForSeconds(0.025f);

        }


    }

}
