﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MoneyEarnedPopUp : MonoBehaviour {

    public CanvasGroup canvasGroup;
    public Text text;


    private void Start()
    {
        
    }

    void Update() {

        DestroyAfterEffect();

    }

    private void DestroyAfterEffect()
    {
        if (canvasGroup.alpha <= 0)
            Destroy(gameObject);
    }

    public void SetPopUpText(int money)
    {

        text.text = ("+ $" + money.ToString());

    }

   
}
