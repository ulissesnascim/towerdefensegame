﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public float startSpeed = 10f;
    [HideInInspector]
    public float speed;

    [HideInInspector]
    public float health;

    public float startHealth = 100f;

    public int moneyGained = 50;

    [Header ("Unity Fields")]
    public GameObject deathEffect;
    public GameObject moneyPopUpPrefab;
    public Image healthBar;

    private bool isDead = false;

    void Start()
    {
        speed = startSpeed;
        health = startHealth;
    }

    public void TakeDamage (float amount)
    {
        health -= amount;

        healthBar.fillAmount = health/startHealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow(float pct)
    {
        speed = startSpeed * (1f - pct);
    }

    void Die ()
    {
        isDead = true;
        PlayerStats.Money += moneyGained;
        PlayerStats.EnemiesKilled += 1;

        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);

        GameObject moneyPopUp = (GameObject)Instantiate(moneyPopUpPrefab, transform.position, Quaternion.identity);
        moneyPopUp.GetComponent<MoneyEarnedPopUp>().SetPopUpText(moneyGained);
        
        WaveSpawner.enemiesAlive--;

        Destroy(gameObject);
    }
    
}
